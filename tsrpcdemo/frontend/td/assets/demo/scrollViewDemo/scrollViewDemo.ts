import { _decorator, Component, log, Node } from 'cc';
import RecycleScroll from '../../script/framework/recycleScrollView/RecycleScroll';
import { ScrollItem } from './ScrollItem';
const { ccclass, property } = _decorator;

@ccclass('Scene')
export class Scene extends Component {
    @property(RecycleScroll)
    scrollView: RecycleScroll = null;

    onLoad() {
        this.scrollView.onItemRender = this.onItemRender.bind(this);
        this.scrollView.onItemClicked = this.onItemClicked.bind(this);

    }

    protected start(): void {
        this.scrollView.numItems = 10000;
    }

    onItemRender(index: number, node: Node) {
        node.getComponent(ScrollItem).setData(index);
    }

    onItemClicked(index: number, node: Node) {
        log(`click: ${index}`)
    }
    
}

