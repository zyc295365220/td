import { _decorator } from 'cc';
import { UIComponent } from '../../script/framework/base/UIComponent';
import { EEventType, EventManager } from '../../script/framework/EventManager';
import { LayerManager } from '../../script/framework/LayerManager';
const { ccclass, property } = _decorator;
/** demo弹窗 */
@ccclass('popDemo')
export class popDemo extends UIComponent {

    onLoad() {
        // this.closeMaskEvent()
        // this.hideMask()
        this.bindView()
        this.init()
        this.initButton()
        this.initEvent()
    }

    /** 弹窗打开调用的函数,每次打开都会调用,在onLoad函数之后 */
    onAdded() {
        this.render()
    }

    /** 一般做ui初始化,设置ui默认状态 */
    init() { }

    /** 做渲染ui,刷新ui */
    render() { }

    /** 初始化按钮点击事件 */
    initButton() {
        // 绑定名字为‘Label’节点点击事件
        this.bindClick(this.view.Label, () => {
            EventManager.Ins.emit(EEventType.test)
        })
    }

    /** 初始化事件监听，接收消息 */
    initEvent() {
        // 注册点击事件，节点被销毁时自动解绑
        EventManager.Ins.on(EEventType.test, () => {
            LayerManager.Ins.toast('点击事件')
        }, this)
    }

}