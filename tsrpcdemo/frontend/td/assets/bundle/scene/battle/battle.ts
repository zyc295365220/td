import { _decorator, NodeEventType, EventTouch, Node, Vec3 } from 'cc';
import { UIComponent } from '../../../script/framework/base/UIComponent';
import { EArea } from '../../../script/src/shared/common/Enum';
import { RenderManager } from '../../../script/gameRender/RenderManager';
import { LayerManager } from '../../../script/framework/LayerManager';
import { UIID } from '../../../script/config/GameUIConfig';
import { GameManager } from '../../../script/runtime/GameManager';
import { TimeUtil } from '../../../script/utils/TimeUtils';
const { ccclass, property } = _decorator;

@ccclass('battle')
export class battle extends UIComponent {

    onLoad() {
        this.bindView()
        this.closeMaskEvent()
        RenderManager.Ins.barArea = this.view.barArea
        RenderManager.Ins.shadowArea = this.view.shadowArea
        RenderManager.Ins.towerArea = this.view.towerArea
        RenderManager.Ins.enemyArea = this.view.enemyArea
        RenderManager.Ins.numbersArea = this.view.numbersArea

    }

    async start() {
        this.bindClick(this.view.btnTest1, () => { this.onJoinRoom(EArea.down) })
        this.bindClick(this.view.btnTest2, () => { this.onJoinRoom(EArea.up) })
        this.bindClick(this.view.btn_drawTower, () => { this.onBuyTower() })
        this.bindClick(this.view.btn_drawCard, () => { this.onShowDrawCard() })
        this.bindClick(this.view.btn_upgrade, () => { this.onUpgrade() })

        this.initOperatBtn()
    }

    update() {
        this.renderUI()
    }

    /** 强化 */
    onUpgrade() {
        LayerManager.Ins.openAsync(UIID.towerUpgrade)
    }

    /** 打开抽卡页面 */
    onShowDrawCard() {
        LayerManager.Ins.openAsync(UIID.towerLuckDraw)
    }

    /** 购买塔 */
    onBuyTower() {
        GameManager.Ins.buyTower()
    }

    /** 加入房间 */
    onJoinRoom(area: EArea) {
        this.view.btnTest1.active = false
        this.view.btnTest2.active = false
        GameManager.Ins.JoinRoom(area)
    }

    renderUI() {
        const { area, gameData } = GameManager.Ins
        if (!gameData) return
        const curUser = gameData.users.find(v => v.area == area)
        const myTowers = gameData.towers.filter(v => v.area == area)
        this.view.lab_round.uiLabel.string = '回合:' + gameData.levelData.curRound
        this.view.lab_time.uiLabel.string = TimeUtil.formatSecondsToMMSS(Math.ceil(gameData.levelData.roundCountdown))
        this.view.lab_number.uiLabel.string = '' + gameData.enemies.length + '/' + gameData.levelData.numberlimit
        this.view.lab_currency1.uiLabel.string = '金币:' + curUser.gold
        this.view.lab_currency2.uiLabel.string = '木材:' + curUser.wood
        this.view.lab_currency3.uiLabel.string = '人口:' + myTowers.length + '/' + curUser.towerNumMax

        this.view.lab_cost.uiLabel.string = '' + (curUser.callNumber * 2 + 20)
    }

    /** 操作区域 */
    initOperatBtn() {
        for (let i = 1; i <= 6; i++) {
            for (let j = 1; j <= 3; j++) {
                this.operatBtn(i, j)
            }
        }
    }

    /** 选择的防御塔i索引 */
    selectI = 0
    /** 选择的防御塔j索引 */
    selectJ = 0
    /** 结束的格子i索引 */
    endI = 0
    /** 结束的格子j索引 */
    endJ = 0
    /** 操作按钮事件 */
    operatBtn(i: number, j: number) {

        const btn = this.view['d' + i + j]
        btn.on(NodeEventType.TOUCH_START, (e: EventTouch) => {
            const { x, y } = e.touch.getUILocation()
            const { indexX, indexY } = this.getTouchIndex(x, y)
            this.touchstart(indexX, indexY)
        })

        btn.on(NodeEventType.TOUCH_MOVE, (e: EventTouch) => {
            const { x, y } = e.touch.getUILocation()
            const { indexX, indexY } = this.getTouchIndex(x, y)
            this.touchmove(indexX, indexY)
        })

        btn.on(NodeEventType.TOUCH_END, (e: EventTouch) => {
            const { x, y } = e.touch.getUILocation()
            const { indexX, indexY } = this.getTouchIndex(x, y)
            this.touchend(indexX, indexY)
        })

        btn.on(NodeEventType.TOUCH_CANCEL, (e: EventTouch) => {
            const { x, y } = e.touch.getUILocation()
            const { indexX, indexY } = this.getTouchIndex(x, y)
            this.touchend(indexX, indexY)
        })
    }

    touchstart(indexX: number, indexY: number) {
        this.selectI = indexX
        this.selectJ = indexY
        this.endI = indexX
        this.endJ = indexY
        const tower = GameManager.Ins.getTower(GameManager.Ins.area, this.selectI, this.selectJ)
        if (!tower) {
            this.selectI = 0
            this.selectJ = 0
        }
    }

    touchmove(indexX: number, indexY: number) {
        this.view.dChange.active = false
        if (indexX > 6 || indexX < 1 || indexY > 3 || indexY < 1) {// 超过边界
            return
        }
        if (this.selectI == 0 || this.selectJ == 0) {// 点击位置没有塔
            return
        }
        if (this.selectI == indexX && this.selectJ == indexY) {// 没有移动位置
            return
        }
        const node = this.view['d' + indexX + indexY]

        if (node) {
            const wh = 130
            this.view.dChange.active = true
            const posX = (indexX - 3) * wh - wh / 2
            const poxY = (2 - indexY) * wh
            this.view.dChange.setPosition(posX, poxY)
            this.endI = indexX
            this.endJ = indexY
        }
    }

    touchend(x: number, y: number) {
        this.view.dChange.active = false
        if (this.selectI && this.selectJ && this.endI && this.endJ) {
            /** 选择的是自己 */
            if (this.selectI == this.endI && this.selectJ == this.endJ) {
                const tower = GameManager.Ins.getTower(GameManager.Ins.area, this.endI, this.endJ)
                if (tower) {
                    let pos = this.view['d' + this.selectI + this.selectJ].getPosition()
                    pos.y = pos.y - 260
                    const { atkDis } = tower
                    LayerManager.Ins.openAsync(UIID.towerOperat, { pos, indexX: this.selectI, indexY: this.selectJ, atkDis })
                }
            } else { //选择的别的格子
                GameManager.Ins.exchangePos(this.selectJ + this.selectI * 10, this.endJ + this.endI * 10)
            }
        }

        this.selectI = 0; this.selectJ = 0; this.endI = 0; this.endJ = 0;
    }

    /** 点击位置的索引 */
    getTouchIndex(x: number, y: number) {
        const localPos = this.view.tai.uiTransform.convertToNodeSpaceAR(new Vec3(x, y, 1))
        const fixX = localPos.x + 390
        const fixY = localPos.y + 185
        const wh = 130
        const indexX = Math.ceil(fixX / wh)
        const indexY = 4 - Math.ceil(fixY / wh)
        return { indexX, indexY }
    }

}