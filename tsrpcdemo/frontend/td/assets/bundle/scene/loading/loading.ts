import { _decorator } from 'cc';
import { LayerManager } from '../../../script/framework/LayerManager';
import { UIID } from '../../../script/config/GameUIConfig';
import { UIComponent } from '../../../script/framework/base/UIComponent';
import { ExcelManager } from '../../../script/runtime/ExcelManager';
import ResManager from '../../../script/runtime/ResManager';
import { GameManager } from '../../../script/runtime/GameManager';
import { NetManager } from '../../../script/runtime/NetManager';
const { ccclass, property } = _decorator;

@ccclass('load')
export class loading extends UIComponent {
    onLoad() {
        this.closeMaskEvent()
    }
    async start() {
        await Promise.all([
            ExcelManager.Ins.init(),
            ResManager.Ins.preLoadMaterial(),
            ResManager.Ins.preLoadCommon(),
            NetManager.Ins.init()
        ])
        GameManager.Ins.init()
        LayerManager.Ins.replaceAsync(UIID.loading, UIID.battle)

    }

}