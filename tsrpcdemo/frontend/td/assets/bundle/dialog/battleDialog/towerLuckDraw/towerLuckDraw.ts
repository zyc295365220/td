import { _decorator } from 'cc';
import { UIComponent } from '../../../../script/framework/base/UIComponent';
import { GameManager } from '../../../../script/runtime/GameManager';
import { CBlueCardCost, CBlueCardRate, COrangeCardCost, COrangeCardRate, CPurpleCardCost, CPurpleCardRate } from '../../../../script/src/shared/common/Contsant';
import { EEventType, EventManager } from '../../../../script/framework/EventManager';
import { MsgSDrawCardResoult } from '../../../../script/src/shared/protocols/battle/MsgSDrawCardResoult';
import { LayerManager } from '../../../../script/framework/LayerManager';
const { ccclass, property } = _decorator;
enum Card {
    blue = 3,
    purple = 4,
    orange = 5,
}
@ccclass('towerLuckDraw')
export class towerLuckDraw extends UIComponent {

    onLoad() {
        this.closeMaskEvent()
        this.hideMask()
        this.bindView()
        this.init()
        this.initButton()
        this.initEvent()
    }

    /** 弹窗打开调用的函数,每次打开都会调用,在onLoad函数之后 */
    onAdded() { }
    
    update() {
        this.render()
    }
    /** 一般做ui初始化,设置ui默认状态 */
    init() { }

    /** 做渲染ui,刷新ui */
    render() {

        const user = GameManager.Ins.getMyself()
        this.view.lab_have.uiLabel.string = '木材:' + user.wood

        this.view.lab_rate_blue.uiLabel.string = CBlueCardRate * 100 + '%'
        this.view.lab_rate_purple.uiLabel.string = CPurpleCardRate * 100 + '%'
        this.view.lab_rate_orange.uiLabel.string = COrangeCardRate * 100 + '%'

        this.view.lab_cost_blue.uiLabel.string = '木材:' + CBlueCardCost
        this.view.lab_cost_purple.uiLabel.string = '木材:' + CPurpleCardCost
        this.view.lab_cost_orange.uiLabel.string = '木材:' + COrangeCardCost

    }

    /** 初始化按钮点击事件 */
    initButton() {
        this.bindClick(this.view.btn_close, () => { this.remove() })
        this.bindClick(this.view.btn_blue, () => { this.onDrawCard(Card.blue) })
        this.bindClick(this.view.btn_purple, () => { this.onDrawCard(Card.purple) })
        this.bindClick(this.view.btn_orange, () => { this.onDrawCard(Card.orange) })
    }

    /** 卡牌抽取 */
    onDrawCard(quality: Card) {
        GameManager.Ins.drawCard(quality)
    }

    /** 初始化事件监听，接收消息 */
    initEvent() {

        EventManager.Ins.on(EEventType.dawCardResoult, this.showDrawResoult, this)
    }

    /** 抽奖结果 */
    showDrawResoult(call: MsgSDrawCardResoult) {
        const { area, isSuccess, quality, towerEntity } = call

        // 抽奖结果是自己的
        if (area == GameManager.Ins.getMyself().area) {

            if (isSuccess) {// 成功
                LayerManager.Ins.toast('抽取成功 塔：' + towerEntity)
            } else {
                LayerManager.Ins.toast('抽取失败')
            }

            return
        }

    }
}

