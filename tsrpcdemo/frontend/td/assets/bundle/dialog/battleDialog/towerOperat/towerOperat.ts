import { _decorator, math, Color } from 'cc';
import { UIComponent } from '../../../../script/framework/base/UIComponent';
import { GameManager } from '../../../../script/runtime/GameManager';
import { LayerManager } from '../../../../script/framework/LayerManager';
const { ccclass, property } = _decorator;
/** demo弹窗 */
@ccclass('towerOperat')
export class towerOperat extends UIComponent {
    pos: math.Vec3
    indexX: number
    indexY: number
    onLoad() {
        // this.closeMaskEvent()
        this.bindView()
        this.init()
        this.initButton()
        this.initEvent()
        this.hideMask()
    }

    /** 弹窗打开调用的函数,每次打开都会调用,在onLoad函数之后 */
    onAdded({ pos, indexX, indexY, atkDis }) {
        this.pos = pos
        this.indexX = indexX
        this.indexY = indexY
        this.render()
        this.setCircleRange(atkDis)
    }

    /** 一般做ui初始化,设置ui默认状态 */
    init() { }

    /** 做渲染ui,刷新ui */
    render() {
        this.view.operatNode.setPosition(this.pos)
    }

    /** 初始化按钮点击事件 */
    initButton() {
        this.bindClick(this.view.btn_sell, () => {
            GameManager.Ins.sellTower(this.indexX, this.indexY)
            this.remove()
        })
        this.bindClick(this.view.btn_merge, () => {
            const arr = GameManager.Ins.gameData.towers.filter(v => v.area == GameManager.Ins.area && v.index[0] == this.indexX && v.index[1] == this.indexY)
            if (arr.length < 3) {
                LayerManager.Ins.toast('数量不足')
                return
            }
            GameManager.Ins.mergeTower(this.indexX, this.indexY)
            this.remove()
        })
    }

    /** 初始化事件监听，接收消息 */
    initEvent() { }

    /** 设置攻击范围展示 */
    setCircleRange(r: number) {

        const graphics = this.view.range.uiGraphics
        graphics.lineWidth = 8
        graphics.strokeColor = Color.WHITE

        graphics.arc(0, 0, r, 0, 2 * Math.PI, true)
        graphics.stroke()

        graphics.circle(0, 0, r)

    }
}