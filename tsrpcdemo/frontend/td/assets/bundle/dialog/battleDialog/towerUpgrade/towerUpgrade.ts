import { _decorator } from 'cc';
import { UIComponent } from '../../../../script/framework/base/UIComponent';
import { GameManager } from '../../../../script/runtime/GameManager';
import { CUpWhiteBlueCost, CUpPurpleCost, CUpOrangeRedCost, CUpSummonCost, CUpgradeWhite, CUpgradeBlue, CUpgradePurple, CUpgradeOrange, CUpgradeRed, CDefaultOrangeRate, CUpOrangeRate, CUpPurpleRate, CDefaultPurpleRate, CUpBlueRate, CDefaultBlueRate } from '../../../../script/src/shared/common/Contsant';
import { EUpgradeType } from '../../../../script/src/shared/common/Enum';
import { getSummonRate } from '../../../../script/src/shared/common/func';
const { ccclass, property } = _decorator;
/** demo弹窗 */
@ccclass('towerUpgrade')
export class towerUpgrade extends UIComponent {

    onLoad() {
        this.bindView()
        this.init()
        this.initButton()
        this.initEvent()
    }

    /** 弹窗打开调用的函数,每次打开都会调用,在onLoad函数之后 */
    onAdded() {
        this.render()
    }

    update() {
        this.render()
    }

    /** 一般做ui初始化,设置ui默认状态 */
    init() { }

    /** 做渲染ui,刷新ui */
    render() {

        const { summonLevel, upgrade1level, upgrade2level, upgrade3level, gold, wood, towerNumMax } = GameManager.Ins.getMyself()

        this.view.lab_whiteBlue.uiLabel.string = 'lv.' + upgrade1level
        this.view.lab_purple.uiLabel.string = 'lv.' + upgrade2level
        this.view.lab_orangeRed.uiLabel.string = 'lv.' + upgrade3level
        this.view.lab_summon.uiLabel.string = summonLevel >= towerNumMax ? 'lv.max' : 'lv.' + summonLevel

        this.view.lab_cost_whiteBlue.uiLabel.string = '金币:' + CUpWhiteBlueCost * upgrade1level
        this.view.lab_cost_purple.uiLabel.string = '金币:' + CUpPurpleCost * upgrade2level
        this.view.lab_cost_orangeRed.uiLabel.string = '木材:' + CUpOrangeRedCost * upgrade3level
        this.view.lab_cost_summon.uiLabel.string = '金币:' + CUpSummonCost

        this.view.lab_gold.uiLabel.string = '金币:' + gold
        this.view.lab_wood.uiLabel.string = '木材:' + wood

        this.view.lab_whiteTip.uiLabel.string = '白色:每级加' + CUpgradeWhite * 100 + '%'
        this.view.lab_blueTip.uiLabel.string = '蓝色:每级加' + CUpgradeBlue * 100 + '%'
        this.view.lab_purpleTip.uiLabel.string = '紫色:每级加' + CUpgradePurple * 100 + '%'
        this.view.lab_orangeTip.uiLabel.string = '橙色:每级加' + CUpgradeOrange * 100 + '%'
        this.view.lab_redTip.uiLabel.string = '红色:每级加' + CUpgradeRed * 100 + '%'

        const orangeRate = Math.floor(getSummonRate(summonLevel, CUpOrangeRate, CDefaultOrangeRate) * 100) / 100
        const purpleRate = Math.floor(getSummonRate(summonLevel, CUpPurpleRate, CDefaultPurpleRate) * 100) / 100
        const blueRate = Math.floor(getSummonRate(summonLevel, CUpBlueRate, CDefaultBlueRate) * 100) / 100

        this.view.lab_whiteRate.uiLabel.string = '橙色:' + orangeRate + '%'
        this.view.lab_blueRate.uiLabel.string = '紫色:' + purpleRate + '%'
        this.view.lab_purpleRate.uiLabel.string = '蓝色:' + blueRate + '%'
        this.view.lab_orangeRate.uiLabel.string = '白色:' + Math.floor((100 - orangeRate - purpleRate - blueRate) * 100) / 100 + '%'

    }

    /** 初始化按钮点击事件 */
    initButton() {
        this.bindClick(this.view.btn_close, () => { this.remove() })
        this.bindClick(this.view.btn_whiteBlue, () => { GameManager.Ins.upgradeUser(EUpgradeType.whiteBlue) })
        this.bindClick(this.view.btn_purple, () => { GameManager.Ins.upgradeUser(EUpgradeType.purple) })
        this.bindClick(this.view.btn_orangeRed, () => { GameManager.Ins.upgradeUser(EUpgradeType.orangeRed) })
        this.bindClick(this.view.btn_rate, () => { GameManager.Ins.upgradeUser(EUpgradeType.summon) })
    }

    /** 初始化事件监听，接收消息 */
    initEvent() {
        // 注册点击事件，节点被销毁时自动解绑
    }

}