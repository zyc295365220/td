/** 时间工具 */
export class TimeUtil {
    /** 间隔天数 */
    public static daysBetween(time1: number | string | Date, time2: number | string | Date): number {
        if (time2 == undefined || time2 == null) {
            time2 = +new Date();
        }
        let startDate = new Date(time1).toLocaleDateString()
        let endDate = new Date(time2).toLocaleDateString()
        let startTime = new Date(startDate).getTime();
        let endTime = new Date(endDate).getTime();
        let dates = Math.abs((startTime - endTime)) / (1000 * 60 * 60 * 24);
        return dates;
    }

    /** 间隔秒数 */
    public static secsBetween(time1: number, time2: number) {
        if (time2 == undefined || time2 == null) {
            time2 = +new Date();
        }
        let dates = Math.abs((time2 - time1)) / (1000);
        return dates;
    }
    /** 将时间戳转换为 分:秒/00:00  */
    public static formatTimeFromSeconds(seconds: number): string {
        const minutes = Math.floor(seconds / 60); // 计算分钟数，取整
        const remainingSeconds = seconds % 60; // 计算剩余秒数
        const formattedTime = `${String(minutes).padStart(2, '0')}:${String(Math.floor(remainingSeconds)).padStart(2, '0')}`; // 取整保证秒数为整数
        return formattedTime;
    }

    /** 将秒数转换成 00:00 格式的字符串 */
    public static formatSecondsToMMSS(seconds: number) {
        // 计算分钟数
        const minutes = Math.floor(seconds / 60);
        // 计算剩余的秒数
        const remainingSeconds = seconds % 60;

        // 确保分钟和秒都是两位数
        const formattedMinutes = String(minutes).padStart(2, '0');
        const formattedSeconds = String(remainingSeconds).padStart(2, '0');

        // 返回格式化后的字符串
        return `${formattedMinutes}:${formattedSeconds}`;
    }
}