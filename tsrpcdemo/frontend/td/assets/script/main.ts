import { _decorator, Component, DynamicAtlasManager, game, macro, tween } from 'cc';
import { LayerManager } from './framework/LayerManager';
import { UIConfigData, UIID } from './config/GameUIConfig';
import { GUI } from './framework/gui/GUI';
import { AudioManager } from './framework/audio/AudioManager';
import { Logger } from './framework/Logger';
import { EEventType, EventManager } from './framework/EventManager';
import { DEBUG } from './common/Contsant';
const { ccclass, property } = _decorator;

macro.CLEANUP_IMAGE_CACHE = false;
DynamicAtlasManager.instance.enabled = true;
DynamicAtlasManager.instance.maxFrameSize = 512;
DynamicAtlasManager.instance.maxAtlasCount = 3;

@ccclass('main')
export class main extends Component {

    protected onLoad(): void {
        game.frameRate = 60
        this.node.addComponent(GUI)
        this.initLayer()
        this.initAudio()
        this.initHeartbeat()
    }

    async start() {
        LayerManager.Ins.openAsync(UIID.loading)
    }

    initLayer() {
        const layerManager = new LayerManager(this.node)
        layerManager.init(UIConfigData, layerManager)
        if (DEBUG) {
            Logger.logConfig('层级管理器初始化完成')
        }
    }

    initAudio() {
        AudioManager.Ins = LayerManager.Ins.root.addComponent(AudioManager)
        AudioManager.Ins.load()
        if (DEBUG) {
            Logger.logConfig('音频管理器初始化完成')
        }
    }

    initHeartbeat() {
        tween(this.node)
            .repeatForever(
                tween(this.node)
                    .delay(1)
                    .call(() => {
                        EventManager.Ins.emit(EEventType.heartbeat)
                    })
                    .start()
            )
            .start()
    }
}