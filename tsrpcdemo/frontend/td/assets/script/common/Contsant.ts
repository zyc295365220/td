export const DEBUG = false
/** entityShader的颜色 */
export const CEntityShadeColor = {
    /** 本来的颜色 */
    Normal: [255, 255, 255, 255],
    /** 白色描边 */
    WhiteOutLine: [255, 255, 255, 1],
    /** 绿色描边 */
    GreenOutLine: [0, 255, 0, 1],
    /** 蓝色描边 */
    BlueOutLine: [0, 0, 255, 1],
    /** 紫色描边 */
    PurpleOutLine: [70, 0, 130, 1],
    /** 橙色描边 */
    OrangeOutLine: [255, 69, 0, 1],
    /** 红色描边 */
    RedOutLine: [255, 0, 0, 1],
    /** 纯白色/受击效果 */
    White: [255, 255, 255, 50],
    /** 纯白色半透明 */
    WhiteOpacity: [122, 255, 255, 50],
    /** 黄色/雷电效果 */
    Yellow: [255, 0, 255, 50],
    /** 红色/燃烧效果 */
    Red: [0, 255, 255, 255],
    /** 绿色/中毒效果 */
    Green: [255, 0, 255, 255],
    /** 蓝色/冰霜效果 */
    Blue: [255, 255, 0, 255],
}
/** 受击特效持续时长 */
export const CBeHitEffectTime = 0.05