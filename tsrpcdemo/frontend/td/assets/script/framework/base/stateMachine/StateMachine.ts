import { Component, Animation } from 'cc';
import { State } from './State';
export abstract class StateMachine extends Component {

    /** 当前状态 */
    private _currencyState: State = null
    /** 状态参数 */
    params: Map<number, boolean> = new Map()
    /** 状态机 */
    stateMachines: Map<number, State> = new Map()
    /** 动画组件 */
    animationComponent: Animation = null

    getPararams(state: number) {
        return this.params.get(state)
    }

    setParams(state: number, value: boolean) {
        if (this.params.has(state)) {
            this.params.set(state, value)
            this.run()
            this.resetTrigger()
        }
    }

    get currencyState() {
        return this._currencyState
    }

    set currencyState(arg: State) {
        if (!arg) return
        this._currencyState = arg
        this._currencyState.run()
    }

    resetTrigger() {
        for (const key in this.params) {
            this.params.set(key as any, false)
        }
    }

    abstract init(...arg: any[]): void
    abstract run(): void
}