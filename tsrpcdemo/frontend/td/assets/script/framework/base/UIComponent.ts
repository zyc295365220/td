import { Node, NodeEventType } from 'cc';
import { Logger } from '../Logger';
import { getComponentName } from '../../utils/Util';
import { UIID } from '../../config/GameUIConfig';
import { LayerManager } from '../LayerManager';
import { UIBase } from './UIBase';
import { Color } from 'cc';

/** 弹窗的基类 */
export class UIComponent extends UIBase {

    /** 点击蒙层是否关闭页面,默认true */
    private _clickmMaskCloseUI: boolean = true
    /** 透明蒙层 */
    mask!: Node
    /** ui根节点 */
    main!: Node
    uid: UIID

    /** ui每次被展示出来之前调用,在onLoad之后 start之前调用 */
    onAdded(_args: any) {
        // if (DEBUG) Logger.logView(`UI:${getComponentName(this)} 展示`)
    }
    /** 生命周期onDestroy之前调用 */
    onBeforeRemove() {
        // if (DEBUG) Logger.logView(`UI:${getComponentName(this)} 移除`)
    }
    /** 生命周期onDestroy之后调用 */
    onRemoved() { }
    /** 透明蒙层点击事件 */
    onMask() {
        if (!this._clickmMaskCloseUI) return
        // if (DEBUG) Logger.logView(`UI:${getComponentName(this)} 蒙层点击事件`)
        this.remove()
    }

    /** 建议使用 remove */
    protected onDestroy(): void {
        super.onDestroy()
        // if (DEBUG) Logger.logView(`UI:${getComponentName(this)} 销毁`)
    }

    /** 移除当前页面 */
    remove(isDestroy?: boolean) {
        LayerManager.Ins.remove(this.uid, isDestroy)
    }

    /** 架构调用 */
    _initUIConfig(uid: UIID) {
        this.uid = uid
        this.main = this.node.parent
        this.mask = this.main.parent.getChildByName('mask')
        this.mask.on(NodeEventType.TOUCH_END, this.onMask, this)
    }

    /** 关闭蒙层点击事件 */
    closeMaskEvent() {
        this._clickmMaskCloseUI = false
    }

    /** 不显示蒙层 */
    hideMask() {
        this.mask.uiSprite.color = new Color(0, 0, 0, 0)
    }

}