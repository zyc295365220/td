import { _decorator, Component, Node, Sprite } from 'cc';
import { StateMachine } from './stateMachine/StateMachine';
import { createUINode } from '../../utils/Util';
import ResManager from '../../runtime/ResManager';

/** 材质类型 */
export enum EMaterialType {
    /** 全屏危险效果材质 */
    dangerMaterial = 'dangerMaterial',
    /** 自定义实体材质 */
    entityMaterial = 'entityMaterial',
    /** 马赛克 */
    mosaicmaterial = 'mosaicmaterial',
}

export abstract class EntityManager extends Component {

    /** 实体类型 */
    abstract entityType: number
    abstract init(...arg: any): void
    /** 渲染 */
    abstract render(...arg: any): void

    /** 状态机 */
    fsm: StateMachine
    private _state: number
    get state() { return this._state }
    set state(arg) {
        this._state = arg
        this.fsm.setParams(this._state, true)
    }

    body: Node

    addBody() {
        this.body = createUINode('body')
        this.body.parent = this.node
        const sp = this.body.addComponent(Sprite)
        sp.sizeMode = Sprite.SizeMode.RAW
        sp.type = Sprite.Type.SIMPLE
        sp.trim = false
    }

    /** 设置body的材质 */
    setBodyMaterial(type = EMaterialType.entityMaterial) {
        const material = ResManager.Ins.getMaterialByType(type)
        const sprite = this.body.getComponent(Sprite)
        sprite.customMaterial = material
    }

}