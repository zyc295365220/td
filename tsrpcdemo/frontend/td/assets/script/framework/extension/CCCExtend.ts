import { EPSILON, Node, RenderData, StencilManager, UIRenderer, approx, clamp, gfx } from "cc";
/** 模板状态类型枚举。 */
enum Stage {
    // Stencil disabled：禁用模板缓冲。在此状态下，不会对绘制区域进行任何限制。
    DISABLED = 0,
    // Clear stencil buffer：清除模板缓冲。将模板缓冲中的所有值都设置为0。
    CLEAR = 1,
    // Entering a new level, should handle new stencil：进入一个新的层级，需要处理新的模板缓冲。在这个状态下，可以根据需要设置新的模板缓冲值。
    ENTER_LEVEL = 2,
    // In content：在内容区域内。绘制操作将受到模板缓冲中的值的限制，只有与模板缓冲值匹配的像素才会被绘制。
    ENABLED = 3,
    // Exiting a level, should restore old stencil or disable：退出一个层级，需要恢复旧的模板缓冲或禁用模板缓冲。在这个状态下，可以根据需要恢复旧的模板缓冲值或禁用模板缓冲。
    EXIT_LEVEL = 4,
    // Clear stencil buffer & USE INVERTED：清除模板缓冲并使用反转模式。将模板缓冲中的所有值都设置为0，并使用反转模式进行绘制。
    CLEAR_INVERTED = 5,
    // Entering a new level & USE INVERTED：进入一个新的层级并使用反转模式。在这个状态下，可以根据需要设置新的模板缓冲值，并使用反转模式进行绘制。
    ENTER_LEVEL_INVERTED = 6,
}

/** cocos 扩展 */
export default class CCCExtend {
    static init() {
        this._extendRender3_x();
        // this.extendAnimationState()
    }

    private static _extendRender3_x() {
        //@ts-ignore
        const batch2d = globalThis['cc'][`internal`][`Batcher2D`];
        let __renderQueue: Node[][] = [];

        const levelSplit = (node: Node, lv: number, itemIndex) => {
            if (!__renderQueue[lv]) {
                __renderQueue[lv] = [];
            }
            __renderQueue[lv].push(node);
            lv++;
            node["__renderLv"] = lv;
            node["__levelRender"] = true;
            node["__itemIndex"] = itemIndex;
            const cs = node.children;
            for (let i = 0; i < cs.length; ++i) {
                const c = cs[i];
                if (!__renderQueue[lv]) {
                    __renderQueue[lv] = [];
                }
                lv = levelSplit(c, lv, itemIndex);
            }
            return lv;
        }
        Object.defineProperty(batch2d.prototype, "walk", {
            value: function (node: Node, level = 0) {
                if (!node[`activeInHierarchy`]) {
                    return;
                }
                const children = node.children;
                const uiProps = node._uiProps;
                const render = uiProps.uiComp as UIRenderer;
                // Save opacity
                let parentOpacity = 1;
                if (node.parent) {
                    parentOpacity = node.parent._uiProps.opacity;
                }
                let opacity = parentOpacity;
                //  在移除ui属性之前，始终级联该属性的局部不透明度
                const selfOpacity = render && render.color ? render.color.a / 255 : 1;

                opacity *= selfOpacity * uiProps.localOpacity;
                //  在移除ui属性之前，将不透明度设置为其不透明度
                // uiProps[`applyOpacity`](opacity);
                if (!approx(opacity, 0, EPSILON)) {
                    if (uiProps.colorDirty) {
                        // 级联颜色脏状态
                        this._opacityDirty++;
                    }

                    // 呈现汇编程序更新逻辑
                    if (render && render.enabledInHierarchy) {
                        render.fillBuffers(this);// for rendering
                    }

                    // 将级联不透明度更新到顶点缓冲区
                    if (this._opacityDirty && render && !render.useVertexOpacity && render.renderData && render.renderData.vertexCount > 0) {
                        // HARD COUPLING
                        updateOpacity(render.renderData, opacity);
                        const buffer = render.renderData.getMeshBuffer();
                        if (buffer) {
                            buffer.setDirty();
                        }
                    }

                    if (children.length > 0 && !node._static) {
                        if (!node[`__levelRender`]) {
                            __renderQueue = [];
                            for (let i = 0; i < children.length; ++i) {
                                const child = children[i];
                                const enableLevelRender = node[`__enableLevelRender`];
                                if (!enableLevelRender) {
                                    this.walk(child, level);
                                } else {
                                    levelSplit(child, 0, i);
                                }
                            }
                            while (__renderQueue.length > 0) {
                                const list = __renderQueue.shift();
                                if (list.length > 0) {
                                    while (list.length > 0) {
                                        const n = list.shift();
                                        this.walk(n, level);
                                    }
                                }
                            }
                        }
                    }

                    if (uiProps.colorDirty) {
                        // Reduce cascaded color dirty state
                        this._opacityDirty--;
                        // Reset color dirty
                        uiProps.colorDirty = false;
                    }
                }
                // Restore opacity
                // this._pOpacity = parentOpacity;

                //渲染后汇编程序更新逻辑
                //注意：还会重置postUpdateAssembler内部的colorDirty
                if (render && render.enabledInHierarchy) {
                    render.postUpdateAssembler(this);
                    if (
                        (
                            render.stencilStage as any === Stage.ENTER_LEVEL ||
                            render.stencilStage as any === Stage.ENTER_LEVEL_INVERTED
                        )
                        && (StencilManager.sharedManager!.getMaskStackSize() > 0)
                    ) {
                        this.autoMergeBatches(this._currComponent!);
                        this.resetRenderStates();
                        StencilManager.sharedManager!.exitMask();
                    }
                }
                level += 1;
            }
        });
    }

    // /** 扩展帧动画播放速度，受游戏速度影响 */
    // private static extendAnimationState() {
    //     const animationState = cclegacy['AnimationState']['prototype']
    //     Object.defineProperty(animationState, "update", {
    //         value: function (delta: number) {
    //             // calculate delay time
    //             delta = delta //* DataManager.Ins.state.gameSpeed
    //             if (this._delayTime > 0.0) {
    //                 this._delayTime -= delta;
    //                 if (this._delayTime > 0.0) {
    //                     // still waiting
    //                     return;
    //                 }
    //             }

    //             // make first frame perfect

    //             // var playPerfectFirstFrame = (this.time === 0);
    //             if (this._currentFramePlayed) {
    //                 this.time += (delta * this._speed);
    //             } else {
    //                 this._currentFramePlayed = true;
    //             }

    //             this._process();
    //         }
    //     })
    // }
}

export function updateOpacity(renderData: RenderData, opacity: number) {
    const vfmt = renderData.vertexFormat;
    const vb = renderData.chunk.vb;
    let attr; let format; let stride;
    // Color component offset
    let offset = 0;
    for (let i = 0; i < vfmt.length; ++i) {
        attr = vfmt[i];
        format = gfx.FormatInfos[attr.format];
        if (format.hasAlpha) {
            stride = renderData.floatStride;
            if (format.size / format.count === 1) {
                const alpha = ~~clamp(Math.round(opacity * 255), 0, 255);
                // Uint color RGBA8
                for (let color = offset; color < vb.length; color += stride) {
                    vb[color] = ((vb[color] & 0xffffff00) | alpha) >>> 0;
                }
            } else if (format.size / format.count === 4) {
                // RGBA32 color, alpha at position 3
                for (let alpha = offset + 3; alpha < vb.length; alpha += stride) {
                    vb[alpha] = opacity;
                }
            }
        }
        offset += format.size >> 2;
    }
}
CCCExtend.init()