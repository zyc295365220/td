import { BlockInputEvents, Layers } from "cc";
import { ViewParams } from "./Defines";
import { LayerUI } from "./LayerUI";

/* 弹窗层，允许同时弹出多个窗口 */
export class LayerPopUp extends LayerUI {
    /** 触摸事件阻挡 */
    protected black!: BlockInputEvents;

    constructor(name: string) {
        super(name);
        this.init();
    }

    private init() {
        this.layer = Layers.Enum.UI_2D;
        this.black = this.addComponent(BlockInputEvents);
        this.black.enabled = false;
    }

    protected showUi(vp: ViewParams) {
        super.showUi(vp);

        // 界面加载完成显示时，层级事件阻挡
        this.black.enabled = true;
    }

    protected onHide(vp: ViewParams) {
        super.onHide(vp);

        // 界面关闭后，关闭触摸事件阻挡、关闭触摸非窗口区域关闭、关闭遮罩
        this.setBlackDisable();
    }

    /** 设置触摸事件阻挡 */
    protected setBlackDisable() {
        // 所有弹窗关闭后，关闭事件阻挡功能
        if (this.ui_nodes.size == 0) {
            this.black.enabled = false;
        }
    }

    clear(isDestroy: boolean) {
        super.clear(isDestroy)
        this.black.enabled = false;
    }
}