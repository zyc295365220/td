import { LayerType, UIConfig } from "../framework/LayerManager";

/** 界面唯一标识（方便服务器通过编号数据触发界面打开） */
export enum UIID {
    /** 资源加载界面 */
    loading = 1,
    /** 弹窗demo */
    popDemo,
    // /** 提示弹出窗口 */
    // Alert,
    // /** 确认弹出窗口 */
    // Confirm,
    // /** 加载与延时提示界面 */
    // Netinstable,
    /** 战斗 */
    battle,
    /** 塔操作 */
    towerOperat,
    /** 战斗卡片抽取 */
    towerLuckDraw,
    /** 塔强化 */
    towerUpgrade,
}

/** 打开界面方式的配置数据 */
export const UIConfigData: { [key: number]: UIConfig } = {
    [UIID.popDemo]: { prefab: "popDemo", bundle: 'popDemo', uid: UIID.popDemo, layer: LayerType.PopUp, },
    [UIID.loading]: { prefab: "loading", bundle: 'loading', uid: UIID.loading, layer: LayerType.UI, },
    [UIID.battle]: { prefab: "battle", bundle: 'battle', uid: UIID.battle, layer: LayerType.UI, },
    [UIID.towerOperat]: { prefab: "towerOperat", bundle: 'towerOperat', uid: UIID.towerOperat, layer: LayerType.PopUp, },
    [UIID.towerLuckDraw]: { prefab: "towerLuckDraw", bundle: 'towerLuckDraw', uid: UIID.towerLuckDraw, layer: LayerType.PopUp, },
    [UIID.towerUpgrade]: { prefab: "towerUpgrade", bundle: 'towerUpgrade', uid: UIID.towerUpgrade, layer: LayerType.PopUp, },

}