import { Animation } from "cc";
import { State } from "../../framework/base/stateMachine/State";
import { StateMachine } from "../../framework/base/stateMachine/StateMachine";
import { EStatus } from "../../src/shared/common/Enum";
import { EntityManager } from "../../framework/base/EntityManager";

export class enemyMachine extends StateMachine {

    entity: EntityManager
    init(entity: EntityManager): void {
        this.animationComponent = this.node.addComponent(Animation)
        this.entity = entity
        this.initParam()
        this.initState()
    }

    initParam() {
        this.params.set(EStatus.run, false)
    }

    initState() {
        this.stateMachines.set(EStatus.run, new State(this, this.entity.entityType, EStatus.run))
    }

    run(): void {
        switch (this.currencyState) {
            case this.stateMachines.get(EStatus.run):
                this.currencyState = this.stateMachines.get(EStatus.run)
                break;
            default:
                this.currencyState = this.stateMachines.get(EStatus.run)
                break;
        }
    }

}


