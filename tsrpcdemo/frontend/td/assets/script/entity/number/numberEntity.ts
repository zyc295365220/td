import { _decorator, Label, Font, Color, Vec3, tween, easing } from 'cc';
import { EntityManager } from '../../framework/base/EntityManager';
import { createUINode } from '../../utils/Util';
import { RandomUtils } from '../../utils/RandomUtils';
import { ObjectPoolManager } from '../../framework/ObjectPoolManager';
import { ResLoader } from '../../framework/ResLoader';
import { EEntity } from '../../src/shared/common/Enum';
const { ccclass, property } = _decorator;
/** 飘字管理器 */
@ccclass('muzzleManager')
export class numberEntity extends EntityManager {
    entityType: number;
    lab: Label
    init(type: EEntity): void {
        this.entityType = type
        const labNode = createUINode('lab')
        this.node.addChild(labNode)
        this.lab = labNode.addComponent(Label)
        ResLoader.Ins.loadAsync('fnt/whitenum', Font).then((font) => {
            this.lab.font = font
        })
    }

    render(color: Color, dmg: number | string, x: number, y: number, fontSize = 20) {

        this.lab.color = color
        this.lab.string = '' + dmg
        this.node.x = x
        this.node.y = y
        this.lab.node.x = 0
        this.lab.node.y = 0
        this.lab.fontSize = fontSize

        //const { x: dirX, y: dirY } = RandomUtils.getRandomDirection()
        //const dis = 50 + RandomUtils.getRandom(false) * 50
        const vec = new Vec3(0, 50)//(dirX * dis, dirY * dis)

        const tw = tween(this.lab.node)
        if (color == Color.WHITE) {
            tw.by(0.4, { position: vec }, { easing: easing.quartOut })
        } else if (color == Color.RED) {
            tw.by(0.2, { position: vec }, { easing: easing.quartOut }).delay(0.2)
        }

        tw.call(() => {
            ObjectPoolManager.Ins.poolFree(this)
        }).start()

    }

}