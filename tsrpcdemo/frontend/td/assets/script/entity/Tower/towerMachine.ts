import { Animation, AnimationClip } from "cc";
import { State } from "../../framework/base/stateMachine/State";
import { StateMachine } from "../../framework/base/stateMachine/StateMachine";
import { EStatus } from "../../src/shared/common/Enum";
import { towerEntity } from "./towerEntity";
import { EntityManager } from "../../framework/base/EntityManager";

export class towerMachine extends StateMachine {

    entity: EntityManager
    init(entity: EntityManager): void {
        this.animationComponent = this.node.addComponent(Animation)
        this.entity = entity
        this.initParam()
        this.initState()
        this.initAnimationEvent()
    }

    initParam() {
        this.params.set(EStatus.idle, false)
        this.params.set(EStatus.atk, false)
    }

    initState() {
        this.stateMachines.set(EStatus.idle, new State(this, this.entity.entityType, EStatus.idle))
        this.stateMachines.set(EStatus.atk, new State(this, this.entity.entityType, EStatus.atk, AnimationClip.WrapMode.Normal))
    }

    initAnimationEvent() {
        this.animationComponent.on(Animation.EventType.FINISHED, () => {
            if (this.animationComponent.defaultClip.name == EStatus.atk.toString()) {
                this.getComponent(towerEntity).state = EStatus.idle
            }
        })
    }

    run(): void {
        switch (this.currencyState) {
            case this.stateMachines.get(EStatus.idle):
                if (this.params.get(EStatus.atk)) {
                    this.currencyState = this.stateMachines.get(EStatus.atk)
                }
                break
            default:
                this.currencyState = this.stateMachines.get(EStatus.idle)
                break;
        }
    }

}