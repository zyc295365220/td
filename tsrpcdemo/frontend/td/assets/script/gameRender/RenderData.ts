
import { Node } from "cc"
import { Singleton } from "../framework/base/Singleton"
import { enemyEntity } from "../entity/Enemy/enemyEntity"
import { towerEntity } from "../entity/Tower/towerEntity"
/** 渲染数据 */
export class RenderData extends Singleton {

    public static get Ins() {
        return this.GetInstance<RenderData>()
    }

    enemy2id: { [key: number]: enemyEntity } = {}
    hpBar2id: { [key: number]: Node } = {}
    shadow2id: { [key: number]: Node } = {}
    tower2id: { [key: number]: towerEntity } = {}

    reset() {

    }

}