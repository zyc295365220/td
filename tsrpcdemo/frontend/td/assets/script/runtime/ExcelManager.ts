import { TableProp } from "../excelType/TableProp"
import { TableTower } from "../excelType/TableTower"
import { Singleton } from "../framework/base/Singleton"
import { JsonUtil } from "../utils/JsonUtil"

const name2class = {
    'tableTower': TableTower,
    'tableProp': TableProp,
}
/** 配置表管理 */
export class ExcelManager extends Singleton {

    public static get Ins() {
        return this.GetInstance<ExcelManager>()
    }

    tableTower!: TableTower
    tableProp!: TableProp

    async init() {

        await this.loadJson()
        for (const name in name2class) {
            this[name] = new name2class[name]()
        }

    }

    async loadJson() {
        const arr = []
        for (const name in name2class) {
            arr.push(JsonUtil.loadAsync(name2class[name].TableName))
        }

        await Promise.all(arr)
    }
}