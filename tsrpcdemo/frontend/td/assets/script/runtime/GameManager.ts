import { Singleton } from "../framework/base/Singleton"
import { EEventType, EventManager } from "../framework/EventManager"
import { LayerManager } from "../framework/LayerManager"
import { RenderManager } from "../gameRender/RenderManager"
import { EArea, EUpgradeType } from "../src/shared/common/Enum"
import { IGameData } from "../src/shared/common/Interface"
import { decompressData } from "../utils/Util"
import { NetManager } from "./NetManager"
/** 配置表管理 */
export class GameManager extends Singleton {

    public static get Ins() {
        return this.GetInstance<GameManager>()
    }

    area: EArea
    gameData: IGameData

    init() {
        this.listenMsg()
    }

    listenMsg() {

        NetManager.Ins.socket.listenMsg('battle/SOutput', call => {
            this.gameData = decompressData(call.data)
            RenderManager.Ins.render(this.gameData, this.area)
        })

        NetManager.Ins.socket.listenMsg('battle/SDrawCardResoult', call => {
            EventManager.Ins.emit(EEventType.dawCardResoult, call)
        })
    }

    /** 加入房间 */
    JoinRoom(area: EArea) {
        this.area = area
        NetManager.Ins.socket.sendMsg('battle/CJoinRoom', {
            id: area,
        })
    }

    /** 购买塔 */
    buyTower() {
        if (this.checkTowerLimit()) return
        NetManager.Ins.socket.sendMsg('battle/CBuyTower', {
            id: this.area
        })
    }

    /** 合并塔 */
    mergeTower(indexX: number, indexY: number) {
        NetManager.Ins.socket.sendMsg('battle/CMergeTower', {
            id: this.area,
            indexX, indexY
        })
    }

    /** 塔交换位置 */
    exchangePos(index1: number, index2: number) {
        NetManager.Ins.socket.sendMsg('battle/CExchangePos', {
            id: this.area,
            index1,
            index2,
        })
    }

    /** 出售塔 */
    sellTower(indexX: number, indexY: number) {
        const { id } = this.getTower(this.area, indexX, indexY)
        NetManager.Ins.socket.sendMsg('battle/CSellTower', {
            id: this.area,
            towerId: id,
        })
    }

    /** 获取防御塔数据 */
    getTower(area: EArea, indexX: number, indexY: number) {
        for (const tower of this.gameData.towers) {
            const { index } = tower
            if (tower.area == area && indexX == index[0] && indexY == index[1]) {
                return tower
            }
        }
    }

    /** 战斗中强化角色数据 */
    upgradeUser(type: EUpgradeType) {
        NetManager.Ins.socket.sendMsg('battle/CUpgrade', {
            id: this.area,
            type,
        })
    }

    /** 战斗中抽塔 */
    drawCard(quality: number) {
        if (this.checkTowerLimit()) return
        NetManager.Ins.socket.sendMsg('battle/CDrawCard', {
            id: this.area,
            quality,
        })
    }

    /** 获取自己信息 */
    getMyself() {
        return this.gameData.users.find(v => v.area == this.area)
    }

    /** 防御塔上限检测 */
    checkTowerLimit() {
        const { towerNumMax } = this.getMyself()
        const towers = this.gameData.towers.filter(v => v.area == this.area)
        if (towers.length >= towerNumMax) {
            LayerManager.Ins.toast('人口已满')
            return true
        }
        return false
    }
}