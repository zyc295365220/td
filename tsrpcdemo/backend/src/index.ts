import * as path from "path";
import { WsServer } from "tsrpc";
import { serviceProto } from "./shared/protocols/serviceProto";
import { GameManager } from "./biz/runtime/GameManager";
import { ExcelManager } from "./biz/runtime/ExcelManager";

// Create the Server
export const server = new WsServer(serviceProto, {
    port: 3000,
    // Remove this to use binary mode (remove from the client too)
    json: true,
    logMsg: false,
});

// Initialize before server start
async function init() {
    // Auto implement APIs
    await server.autoImplementApi(path.resolve(__dirname, 'api'));
    ExcelManager.Ins.init()
    GameManager.Ins.init()
};

// Entry function
async function main() {
    await init();
    await server.start();
};
main();