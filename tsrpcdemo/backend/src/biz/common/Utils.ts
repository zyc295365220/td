import { gzipSync } from "zlib"

/** 深拷贝 */
export const deepClone = (obj: any) => {
    if (obj === null || typeof obj !== 'object') {
        return obj
    }
    const res = Array.isArray(obj) ? [] : {} as any
    for (const key in obj) {
        if (Object.prototype.hasOwnProperty.call(obj, key)) {
            res[key] = deepClone(obj[key]) as any
        }
    }
    return res
}

/** 压缩数据 */
export const compressData = (data: any) => {
    try {
        const jsonString = JSON.stringify(data);
        const inputBuffer = Buffer.from(jsonString, 'utf8');
        const compressedData = gzipSync(inputBuffer);
        const base64CompressedData = compressedData.toString('base64');
        return base64CompressedData
    } catch (err) {
        console.error("解压或解析时出错:", err);
        return {}
    }
}