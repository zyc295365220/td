/** 每回合时间 */
export const countDown = 20
/** 出怪cd */
export const bornCD = 0.8
/** 每波怪物数量 */
export const bornNum = 1
/** 怪物移动速度 */
export const moveSpeed = 220
/** 怪物生命 */
export const enemyHp = Number.MAX_SAFE_INTEGER
/** 最大回合 */
export const maxRound = 80
/** 用户初始金币 */
export const userDefaultGold = 100
/** 用户初始木材 */
export const userDefaultWood = 10
/**m 默认人口 */
export const towerNumMax = 20
/** 游戏倍速 */
export const gameSpeed = 1
