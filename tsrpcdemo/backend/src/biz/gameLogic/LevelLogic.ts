import { CBornPos } from "../../shared/common/Contsant";
import { EArea } from "../../shared/common/Enum";
import { IGameData } from "../../shared/common/Interface";
import { Singleton } from "../../shared/framework/base/Singleton";
import { countDown, maxRound } from "../common/Contsant";
import { ExcelManager } from "../runtime/ExcelManager";
import { EnemyLogic } from "./EnemyLogic";

/** 关卡逻辑 */
export class LevelLogic extends Singleton {

    public static get Ins() {
        return this.GetInstance<LevelLogic>()
    }
    /** 需要生成的怪物数量 */
    bornNum = 0
    /** 出怪cd */
    bornCD = 0

    loop(gameData: IGameData) {

        this.loopRound(gameData)
        this.loopGenerateEnemy(gameData)

    }

    /** 回合 */
    loopRound(gameData: IGameData) {
        gameData.levelData.roundCountdown -= gameData.step
        if (gameData.levelData.roundCountdown <= 0) {
            if (maxRound > gameData.levelData.curRound) {
                ++gameData.levelData.curRound
            }
            ExcelManager.Ins.tableEnemy.init(gameData.levelData.curRound)
            gameData.levelData.roundCountdown = countDown
            this.bornNum = ExcelManager.Ins.tableEnemy.bornNum
        }
    }

    /** 怪物生成 */
    loopGenerateEnemy(gameData: IGameData) {
        if (this.bornNum > 0) {
            if (this.bornCD < 0) {
                --this.bornNum
                ExcelManager.Ins.tableEnemy.init(gameData.levelData.curRound)
                this.bornCD = ExcelManager.Ins.tableEnemy.bornCD
                EnemyLogic.Ins.generateEnemy(gameData.roomId, CBornPos.xUp, CBornPos.yUp, EArea.up)
                EnemyLogic.Ins.generateEnemy(gameData.roomId, CBornPos.xDown, CBornPos.yDown, EArea.down)
            } else {
                this.bornCD -= gameData.step
            }
        }
    }



}