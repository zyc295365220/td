import { IGameData } from "../../shared/common/Interface";
import { Singleton } from "../../shared/framework/base/Singleton";
import { EnemyLogic } from "./EnemyLogic";
import { LevelLogic } from "./LevelLogic";
import { TowerLogic } from "./TowerLogic";

/** 游戏逻辑 */
export class GameLogic extends Singleton {

    public static get Ins() {
        return this.GetInstance<GameLogic>()
    }

    /** 主循环之前 */
    gameBeforeLoop(gameData: IGameData) {
        EnemyLogic.Ins.removeDeadedEnemy(gameData.enemies)
        TowerLogic.Ins.removeDeadedTower(gameData)

    }

    /** 游戏主循环 */
    gameMainLoop(gameData: IGameData) {
        EnemyLogic.Ins.loop(gameData)
        TowerLogic.Ins.loop(gameData)
        LevelLogic.Ins.loop(gameData)
    }

    /** 主循环之后 */
    gameLateLoop(gameData: IGameData) {
    }

}