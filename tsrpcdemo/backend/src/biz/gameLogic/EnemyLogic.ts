import { CTurnPos } from "../../shared/common/Contsant";
import { EArea, EStatus } from "../../shared/common/Enum";
import { IEnemy, IGameData, ITower, IUser } from "../../shared/common/Interface";
import { Singleton } from "../../shared/framework/base/Singleton";
import { ExcelManager } from "../runtime/ExcelManager";
import { GameManager } from "../runtime/GameManager";
import { TowerLogic } from "./TowerLogic";

/** 怪物逻辑 */
export class EnemyLogic extends Singleton {

    public static get Ins() {
        return this.GetInstance<EnemyLogic>()
    }

    loop(gameData: IGameData) {
        for (const enemy of gameData.enemies) {
            enemy.beHit = []
            this.enemyMove(enemy, gameData.step)
        }

    }

    /** 怪物移动 */
    enemyMove(enemy: IEnemy, step: number) {
        const { x, y, status } = enemy
        if (status == EStatus.dead) {
            return
        }
        const moveSpeed = ExcelManager.Ins.tableEnemy.moveSpeed
        if (enemy.area == EArea.up) {

            if (x == CTurnPos.leftUpX) {
                enemy.y -= step * moveSpeed
            } else if (y == CTurnPos.leftMidY) {
                enemy.x += step * moveSpeed
            } else if (x == CTurnPos.rightMidX) {
                enemy.y += step * moveSpeed
            } else if (y == CTurnPos.rightUpY) {
                enemy.x -= step * moveSpeed
            }

            if (x == CTurnPos.leftMidX && y < CTurnPos.leftMidY) {
                enemy.y = CTurnPos.leftMidY
                enemy.x = CTurnPos.leftMidX + CTurnPos.leftMidY - y
            } else if (y == CTurnPos.rightMidY && x > CTurnPos.rightMidX) {
                enemy.x = CTurnPos.rightMidX
                enemy.y = CTurnPos.rightMidY + x - CTurnPos.rightMidX
            } else if (x == CTurnPos.rightUpX && y > CTurnPos.rightUpY) {
                enemy.y = CTurnPos.rightUpY
                enemy.x = CTurnPos.rightUpX + CTurnPos.rightUpY - y
            } else if (y == CTurnPos.leftUpY && x < CTurnPos.leftUpX) {
                enemy.x = CTurnPos.leftUpX
                enemy.y = CTurnPos.leftUpY + x - CTurnPos.leftUpX
            }
        } else if (enemy.area == EArea.down) {
            if (x == CTurnPos.leftDownX) {
                enemy.y += step * moveSpeed
            } else if (y == CTurnPos.leftMidY) {
                enemy.x += step * moveSpeed
            } else if (x == CTurnPos.rightMidX) {
                enemy.y -= step * moveSpeed
            } else if (y == CTurnPos.rightDownY) {
                enemy.x -= step * moveSpeed
            }

            if (x == CTurnPos.leftMidX && y > CTurnPos.leftMidY) {
                enemy.y = CTurnPos.leftMidY
                enemy.x = CTurnPos.leftMidX + y - CTurnPos.leftMidY
            } else if (y == CTurnPos.rightMidY && x > CTurnPos.rightMidX) {
                enemy.x = CTurnPos.rightMidX
                enemy.y = CTurnPos.rightMidY + CTurnPos.rightMidX - x
            } else if (x == CTurnPos.rightDownX && y < CTurnPos.rightDownY) {
                enemy.y = CTurnPos.rightDownY
                enemy.x = CTurnPos.rightDownX + y - CTurnPos.rightDownY
            } else if (y == CTurnPos.leftDownY && x < CTurnPos.leftDownX) {
                enemy.x = CTurnPos.leftDownX
                enemy.y = CTurnPos.leftDownY + CTurnPos.leftDownX - x
            }
        }
    }

    /** 受到攻击 */
    beHit(enemy: IEnemy, tower: ITower, users: IUser[]) {
        const atkVal = TowerLogic.Ins.getTowerAtkVal(tower, users)
        const dmg = atkVal
        enemy.curHp -= dmg
        enemy.beHit.push({ dmg, isCri: 0 })
        if (enemy.curHp <= 0) {
            this.enemyDead(enemy, tower, users)
        }
    }

    /** 怪物死亡 */
    enemyDead(enemy: IEnemy, tower: ITower, users: IUser[]) {
        enemy.curHp = 0
        enemy.status = EStatus.dead
        const user = users.find(v => v.area == enemy.area)
        if (user) {
            ExcelManager.Ins.tableEnemy.init(enemy.enemyId)
            user.gold += ExcelManager.Ins.tableEnemy.gold
            user.wood += ExcelManager.Ins.tableEnemy.wood
        }
    }

    /** 生成一个怪物 */
    generateEnemy(roomId: number, x: number, y: number, area: EArea) {
        const room = GameManager.Ins.getRoomById(roomId)
        const enemyHp = ExcelManager.Ins.tableEnemy.hp
        const entityType = ExcelManager.Ins.tableEnemy.entityType
        const enemyId = ExcelManager.Ins.tableEnemy.id
        const enemy: IEnemy = {
            curHp: enemyHp,
            maxHp: enemyHp,
            status: EStatus.running,
            id: room.getNextId(),
            area,
            x,
            y,
            enemyId,
            entityType,
            beHit: [],

        }
        room.gameData.enemies.push(enemy)
    }

    /** 移除死亡怪物 */
    removeDeadedEnemy(enemies: IEnemy[]) {
        for (let i = enemies.length - 1; i >= 0; i--) {
            const enemy = enemies[i]
            const { status } = enemy
            if (status == EStatus.dead) {
                enemies.splice(i, 1)
            }
        }
    }
}