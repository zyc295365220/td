import { BaseConnection, MsgCall } from "tsrpc"
import { EArea, EEntity, EStatus, EUpgradeType } from "../../shared/common/Enum"
import { IUser } from "../../shared/common/Interface"
import { TowerLogic } from "../gameLogic/TowerLogic"
import { RandomUtils } from "../../framework/RandomUtils"
import { CBlueCardCost, CBlueCardRate, COrangeCardCost, COrangeCardRate, CPurpleCardCost, CPurpleCardRate, CSummonMaxLv, CUpOrangeRedCost, CUpPurpleCost, CUpSummonCost, CUpWhiteBlueCost } from "../../shared/common/Contsant"
import { GameManager } from "./GameManager"
import { server } from "../.."
import { MsgCDrawCard } from "../../shared/protocols/battle/MsgCDrawCard"
import { towerNumMax, userDefaultGold, userDefaultWood } from "../common/Contsant"
import { getSummonRate } from "../../shared/common/func"

/** 游戏用户 */
export class GameUser {

    /** 链接状态 */
    connStatus: EStatus = EStatus.conn
    /** 链接 */
    conn: BaseConnection<any> | null = null
    /** 游戏数据 */
    data: IUser
    constructor(area: EArea, call: MsgCall) {
        this.data = {
            userId: call.msg.id,
            area,
            gold: userDefaultGold,
            wood: userDefaultWood,
            callNumber: 0,
            roomId: 0,
            towerNumMax: towerNumMax,
            summonLevel: 1,
            upgrade1level: 1,
            upgrade2level: 1,
            upgrade3level: 1,
            summonLevelMax: CSummonMaxLv
        }
    }

    /** 加入房间 */
    joinRoom(roomId: number, call: MsgCall) {
        this.conn = call.conn
        this.data.roomId = roomId
        this.connStatus = EStatus.conn
    }

    /** 断开链接 */
    disconn() {
        this.conn = null
        this.connStatus = EStatus.disconn
    }

    /** 召唤防御塔 */
    buyTower() {
        const { roomId, area, towerNumMax, summonLevel } = this.data
        if (TowerLogic.Ins.checkTowerNumMax(roomId, area, towerNumMax)) return
        if (this.checkGold()) return
        const room = GameManager.Ins.getRoomById(roomId)
        const towerE2map = {
            1: [EEntity.tower1, EEntity.tower2, EEntity.tower3, EEntity.tower4, EEntity.tower5],
            3: [EEntity.tower6, EEntity.tower7, EEntity.tower8, EEntity.tower9, EEntity.tower10],
            4: [EEntity.tower11, EEntity.tower12, EEntity.tower13, EEntity.tower14, EEntity.tower15],
            5: [EEntity.tower16, EEntity.tower17, EEntity.tower18, EEntity.tower19],
        }

        const quality = TowerLogic.Ins.getSummonTowerQuality(summonLevel)
        const towers = towerE2map[quality]

        const randomIndex = RandomUtils.getRandomInt(0, towers.length, true, 1)
        const entityType = towers[randomIndex]

        const index = TowerLogic.Ins.checkIndex(entityType, roomId, area)
        if (!index) return

        this.data.gold -= this.data.callNumber * 2 + 20
        ++this.data.callNumber
        room.generateTower.push({ area: this.data.area, entity: entityType, roomId })
    }

    /** 金币检测 */
    checkGold() {
        const callNumber = this.data.callNumber
        if (this.data.gold < callNumber * 2 + 20) {
            return true
        }
        return false
    }

    /** 战斗中抽取塔 */
    drawCard(call: MsgCall<MsgCDrawCard, any>) {
        let cost = 0
        let rate = 0
        const quality = call.msg.quality
        switch (quality) {
            case 3:
                cost = CBlueCardCost; rate = CBlueCardRate
                break;
            case 4:
                cost = CPurpleCardCost; rate = CPurpleCardRate
                break;
            case 5:
                cost = COrangeCardCost; rate = COrangeCardRate
                break;
            default:
                console.warn('### error msg drawCard, err quality:', quality)
                return
        }

        /** 钱不够 */
        if (this.data.wood < cost) {
            return
        }

        if (TowerLogic.Ins.checkTowerNumMax(this.data.roomId, this.data.area, this.data.towerNumMax)) return

        this.data.wood -= cost
        const random = RandomUtils.getRandomFloat()

        const room = GameManager.Ins.getRoomById(this.data.roomId)
        const conns = [room.users[0].conn, room.users[1].conn,] as any
        /** 抽取成功 */
        if (random <= rate) {
            const towerEntity = TowerLogic.Ins.getRandomTowerByQuality(quality)
            if (!TowerLogic.Ins.checkIndex(towerEntity, this.data.roomId, this.data.area)) return
            room.drawTower(this.data.userId, towerEntity)


            server.broadcastMsg('battle/SDrawCardResoult', { area: this.data.area, isSuccess: 1, quality, towerEntity }, conns)
            return
        }
        /** 抽取失败 */
        server.broadcastMsg('battle/SDrawCardResoult', { area: this.data.area, isSuccess: 0, quality, towerEntity: 0 }, conns)

    }

    /** 用户强化 */
    upgrade(type: EUpgradeType) {

        switch (type) {
            case EUpgradeType.whiteBlue: {
                const cost = CUpWhiteBlueCost * this.data.upgrade1level
                if (this.data.gold < cost) return
                this.data.gold -= cost
                this.data.upgrade1level += 1
                break;
            }
            case EUpgradeType.purple: {
                const cost = CUpPurpleCost * this.data.upgrade2level
                if (this.data.gold < cost) return
                this.data.gold -= cost
                this.data.upgrade2level += 1
                break;
            }
            case EUpgradeType.orangeRed: {
                const cost = CUpOrangeRedCost * this.data.upgrade3level
                if (this.data.wood < cost) return
                this.data.wood -= cost
                this.data.upgrade3level += 1
                break;
            }
            case EUpgradeType.summon: {
                if (this.data.summonLevel >= this.data.summonLevelMax) return
                if (this.data.gold < CUpSummonCost) return
                this.data.gold -= CUpSummonCost
                this.data.summonLevel += 1
                break;
            }
        }

    }

}