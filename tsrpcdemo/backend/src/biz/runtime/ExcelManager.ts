import { Singleton } from "../../shared/framework/base/Singleton";
import { TableEnemy } from "../excelType/TableEnemy";
import { TableTower } from '../excelType/TableTower';

const name2class: { [key: string]: any } = {
    'tableTower': TableTower,
    'tableEnemy': TableEnemy,
}
/** 配置表管理 */
export class ExcelManager extends Singleton {

    jsonData: any = {}
    public static get Ins() {
        return this.GetInstance<ExcelManager>()
    }

    tableTower!: TableTower
    tableEnemy!: TableEnemy

    init() {
        this.loadJson()
        for (const name in name2class) {
            //@ts-ignore
            this[name as any] = new name2class[name]()
        }
    }

    /** 读取json文件 */
    async loadJson() {
        for (const name in name2class) {
            name2class[name].load()
        }
    }
}