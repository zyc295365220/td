/** 获取召唤概率 */
export const getSummonRate = (level: number, upRate: number, defaultVal: number) => {
    let res = defaultVal
    for (let index = 1; index <= level - 1; index++) {
        res += 0.9 ** index * upRate
    }
    return res
}