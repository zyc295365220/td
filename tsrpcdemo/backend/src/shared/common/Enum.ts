/** 状态 */
export enum EStatus {
    idle,
    /** 暂停 */
    pause,
    /** 运行中,行动中 */
    running,
    /** 死亡 */
    dead,
    /** 攻击 */
    atk,
    /** 移动 */
    run,
    /** 链接断开 */
    disconn,
    /** 链接 */
    conn,
    /** 失败 */
    lost,
    /** 出生 */
    born,
}

/** 实体类型 */
export enum EEntity {
    empty,
    tower1 = 10001,
    tower2,
    tower3,
    tower4,
    tower5,
    tower6 = 10006,
    tower7,
    tower8,
    tower9,
    tower10,
    tower11 = 10011,
    tower12,
    tower13,
    tower14,
    tower15,
    tower16 = 10016,
    tower17,
    tower18,
    tower19,
    enemy1 = 20001,
    enemy2 = 20002,
    enemy3 = 20003,
    enemy4 = 20004,
    enemy5 = 20005,
    enemy6 = 20006,
    enemy7 = 20007,
    enemy8 = 20008,
    enemy9 = 20009,
    enemy10 = 20010,
    battleNumber = 30001,
}

/** 所在场地 */
export enum EArea {
    /** 上半场 */
    up = 1,
    /** 下半场 */
    down,
}

/** 角色数据强化类型 */
export enum EUpgradeType {
    /** 白蓝塔 */
    whiteBlue,
    /** 紫塔 */
    purple,
    /** 橙红塔 */
    orangeRed,
    /** 召唤等级 */
    summon,
}

/** 品质枚举 */
export enum EQuality {
    white = 1,
    green,
    blue,
    purple,
    orange,
    red,
}