import { EArea, EEntity, EStatus } from "./Enum"

/** 游戏数据 */
export interface IGameData {
    roomId: number
    /** 用户信息 */
    users: IUser[],
    /** 怪物信息 */
    enemies: IEnemy[],
    /** 防御塔信息 */
    towers: ITower[],
    /** 关卡信息 */
    levelData: ILevelData,
    /** 下一个id */
    nextId: number,
    /** 当前帧步长,毫秒 */
    step: number,
}

/** 关卡信息 */
export interface ILevelData {
    /** 回合倒计时 */
    roundCountdown: number,
    /** 当前回合 */
    curRound: number,
    /** 难度 */
    difficulty: number,
    /** 怪物数量限制 */
    numberlimit: number,

}
/** 防御塔信息 */
export interface ITower extends ICommonEntity {
    /** 攻击力 */
    atkVal: number,
    /** 攻击方向 0左 1右 */
    atkDir: 0 | 1,
    /** 攻击速度 */
    atkSpd: number,
    /** 攻击计时器 */
    atkTag: number,
    /** 攻击距离 */
    atkDis: number,
    /** 暴击伤害 */
    criDmg: number,
    /** 暴击率 */
    criRat: number,
    /** 目标怪物id */
    tarId: number,
    /** 索引位置 */
    index: [number, number],
}
/** 怪物信息 */
export interface IEnemy extends ICommonEntity {
    /** 当前生命值 */
    curHp: number,
    /** 最大生命值 */
    maxHp: number,
    /** 怪物id 对应enemy表 */
    enemyId: number,
    /** 受到攻击 */
    beHit: { dmg: number, isCri: 0 | 1 }[],
}

/** 用户信息 */
export interface IUser {
    userId: number,
    /** 所在房间id */
    roomId: number,
    /** 所在半场 */
    area: EArea,
    /** 金币 */
    gold: number,
    /** 木材 */
    wood: number,
    /** 已经召唤防御塔的次数 */
    callNumber: number,
    /** 防御塔数量上限 */
    towerNumMax: number,
    /** 召唤等级 */
    summonLevel: number,
    /** 召唤等级满级 */
    summonLevelMax: number,
    /** 白蓝强化卡等级 */
    upgrade1level: number,
    /** 紫卡强化等级 */
    upgrade2level: number,
    /** 橙红卡强化等级 */
    upgrade3level: number,


}

/** 通用属性 */
interface ICommonEntity {
    status: EStatus
    id: number,
    /** 所属半场 */
    area: EArea,
    x: number,
    y: number,
    /** 防御塔类型 */
    entityType: EEntity
}