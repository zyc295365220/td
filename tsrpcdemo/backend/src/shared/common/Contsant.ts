/** 转弯坐标 */
export const CTurnPos = {
    /** 左上拐弯点 */
    leftUpX: -455,
    leftUpY: 520,
    /** 右上拐弯点 */
    rightUpX: 455,
    rightUpY: 520,
    /** 左中拐弯点 */
    leftMidX: -455,
    leftMidY: 0,
    /** 右中拐弯点 */
    rightMidX: 455,
    rightMidY: 0,
    /** 左下拐弯点 */
    leftDownX: -455,
    leftDownY: -520,
    /** 右下拐弯点 */
    rightDownX: 455,
    rightDownY: -520,

}

/** 出怪坐标 */
export const CBornPos = {
    /** 上半场出生点 */
    xUp: -455,
    yUp: 540,
    /** 下半场出生点 */
    xDown: -455,
    yDown: -560,
}

/** 上半区域 */
export const CUpArea: { [key: string]: number } = {
    /** 近出怪点 */
    y3: 390,
    y2: 260,
    /** 远出怪点 */
    y1: 130,

    x1: -325,
    x2: -195,
    x3: -65,
    x4: 65,
    x5: 195,
    x6: 325,
}

/** 下半区域 */
export const CDownArea: { [key: string]: number } = {
    /** 远出怪点 */
    y1: -130,
    y2: -260,
    /** 近出怪点 */
    y3: -390,

    x1: -325,
    x2: -195,
    x3: -65,
    x4: 65,
    x5: 195,
    x6: 325,
}

/** 怪物最大数量，最大游戏失败 */
export const CMaxEnemyNum = 100

/** 抽卡，蓝卡抽取概率 */
export const CBlueCardRate = 0.6
/** 抽卡，蓝卡抽取消耗 */
export const CBlueCardCost = 1
/** 紫卡 */
export const CPurpleCardRate = 0.2
/** 紫卡 */
export const CPurpleCardCost = 1
/** 橙卡 */
export const COrangeCardRate = 0.1
/** 橙卡 */
export const COrangeCardCost = 2

/** 战斗中强化，白卡每次提升的攻击力 */
export const CUpgradeWhite = 1
/** 战斗中强化，蓝每次提升的攻击力 */
export const CUpgradeBlue = 0.75
/** 战斗中强化，紫卡每次提升的攻击力 */
export const CUpgradePurple = 0.6
/** 战斗中强化，橙卡每次提升的攻击力 */
export const CUpgradeOrange = 0.5
/** 战斗中强化，红卡每次提升的攻击力 */
export const CUpgradeRed = 0.5

/** 橙卡召唤概率 */
export const CDefaultOrangeRate = 0.1
/** 紫卡召唤概率 */
export const CDefaultPurpleRate = 0.48
/** 蓝卡召唤概率 */
export const CDefaultBlueRate = 3.89

/** 橙卡召唤概率成长系数 */
export const CUpOrangeRate = 0.34
/** 紫卡召唤概率成长系数 */
export const CUpPurpleRate = 1.73
/** 蓝卡召唤概率成长系数 */
export const CUpBlueRate = 5.16

/** 白蓝卡升级消耗，金币 */
export const CUpWhiteBlueCost = 30
/** 紫卡升级消耗，金币 */
export const CUpPurpleCost = 50
/** 橙红卡升级消耗，木材 */
export const CUpOrangeRedCost = 1
/** 召唤升级金币消耗 */
export const CUpSummonCost = 100
/** 召唤等级满级 */
export const CSummonMaxLv = 10