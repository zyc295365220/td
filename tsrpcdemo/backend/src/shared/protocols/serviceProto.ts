import { ServiceProto } from 'tsrpc-proto';
import { MsgCBuyTower } from './battle/MsgCBuyTower';
import { MsgCDrawCard } from './battle/MsgCDrawCard';
import { MsgCExchangePos } from './battle/MsgCExchangePos';
import { MsgCJoinRoom } from './battle/MsgCJoinRoom';
import { MsgCMergeTower } from './battle/MsgCMergeTower';
import { MsgCSellTower } from './battle/MsgCSellTower';
import { MsgCUpgrade } from './battle/MsgCUpgrade';
import { MsgSDrawCardResoult } from './battle/MsgSDrawCardResoult';
import { MsgSOutput } from './battle/MsgSOutput';
import { MsgStr } from './MsgStr';
import { ReqAddData, ResAddData } from './PtlAddData';
import { ReqGetData, ResGetData } from './PtlGetData';

export interface ServiceType {
    api: {
        "AddData": {
            req: ReqAddData,
            res: ResAddData
        },
        "GetData": {
            req: ReqGetData,
            res: ResGetData
        }
    },
    msg: {
        "battle/CBuyTower": MsgCBuyTower,
        "battle/CDrawCard": MsgCDrawCard,
        "battle/CExchangePos": MsgCExchangePos,
        "battle/CJoinRoom": MsgCJoinRoom,
        "battle/CMergeTower": MsgCMergeTower,
        "battle/CSellTower": MsgCSellTower,
        "battle/CUpgrade": MsgCUpgrade,
        "battle/SDrawCardResoult": MsgSDrawCardResoult,
        "battle/SOutput": MsgSOutput,
        "Str": MsgStr
    }
}

export const serviceProto: ServiceProto<ServiceType> = {
    "version": 26,
    "services": [
        {
            "id": 14,
            "name": "battle/CBuyTower",
            "type": "msg"
        },
        {
            "id": 15,
            "name": "battle/CDrawCard",
            "type": "msg"
        },
        {
            "id": 16,
            "name": "battle/CExchangePos",
            "type": "msg"
        },
        {
            "id": 17,
            "name": "battle/CJoinRoom",
            "type": "msg"
        },
        {
            "id": 18,
            "name": "battle/CMergeTower",
            "type": "msg"
        },
        {
            "id": 19,
            "name": "battle/CSellTower",
            "type": "msg"
        },
        {
            "id": 22,
            "name": "battle/CUpgrade",
            "type": "msg"
        },
        {
            "id": 20,
            "name": "battle/SDrawCardResoult",
            "type": "msg"
        },
        {
            "id": 21,
            "name": "battle/SOutput",
            "type": "msg"
        },
        {
            "id": 6,
            "name": "Str",
            "type": "msg"
        },
        {
            "id": 0,
            "name": "AddData",
            "type": "api"
        },
        {
            "id": 1,
            "name": "GetData",
            "type": "api"
        }
    ],
    "types": {
        "battle/MsgCBuyTower/MsgCBuyTower": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "id",
                    "type": {
                        "type": "Number"
                    }
                }
            ]
        },
        "battle/MsgCDrawCard/MsgCDrawCard": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "id",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 1,
                    "name": "quality",
                    "type": {
                        "type": "Number"
                    }
                }
            ]
        },
        "battle/MsgCExchangePos/MsgCExchangePos": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "id",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 1,
                    "name": "index1",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 2,
                    "name": "index2",
                    "type": {
                        "type": "Number"
                    }
                }
            ]
        },
        "battle/MsgCJoinRoom/MsgCJoinRoom": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "id",
                    "type": {
                        "type": "Number"
                    }
                }
            ]
        },
        "battle/MsgCMergeTower/MsgCMergeTower": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "id",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 1,
                    "name": "indexX",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 2,
                    "name": "indexY",
                    "type": {
                        "type": "Number"
                    }
                }
            ]
        },
        "battle/MsgCSellTower/MsgCSellTower": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "id",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 1,
                    "name": "towerId",
                    "type": {
                        "type": "Number"
                    }
                }
            ]
        },
        "battle/MsgCUpgrade/MsgCUpgrade": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "id",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 1,
                    "name": "type",
                    "type": {
                        "type": "Reference",
                        "target": "../common/Enum/EUpgradeType"
                    }
                }
            ]
        },
        "../common/Enum/EUpgradeType": {
            "type": "Enum",
            "members": [
                {
                    "id": 0,
                    "value": 0
                },
                {
                    "id": 1,
                    "value": 1
                },
                {
                    "id": 2,
                    "value": 2
                },
                {
                    "id": 3,
                    "value": 3
                }
            ]
        },
        "battle/MsgSDrawCardResoult/MsgSDrawCardResoult": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "area",
                    "type": {
                        "type": "Reference",
                        "target": "../common/Enum/EArea"
                    }
                },
                {
                    "id": 1,
                    "name": "isSuccess",
                    "type": {
                        "type": "Union",
                        "members": [
                            {
                                "id": 0,
                                "type": {
                                    "type": "Literal",
                                    "literal": 0
                                }
                            },
                            {
                                "id": 1,
                                "type": {
                                    "type": "Literal",
                                    "literal": 1
                                }
                            }
                        ]
                    }
                },
                {
                    "id": 2,
                    "name": "quality",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 4,
                    "name": "towerEntity",
                    "type": {
                        "type": "Reference",
                        "target": "../common/Enum/EEntity"
                    }
                }
            ]
        },
        "../common/Enum/EArea": {
            "type": "Enum",
            "members": [
                {
                    "id": 1,
                    "value": 1
                },
                {
                    "id": 2,
                    "value": 2
                }
            ]
        },
        "../common/Enum/EEntity": {
            "type": "Enum",
            "members": [
                {
                    "id": 0,
                    "value": 0
                },
                {
                    "id": 7,
                    "value": 10001
                },
                {
                    "id": 13,
                    "value": 10002
                },
                {
                    "id": 14,
                    "value": 10003
                },
                {
                    "id": 15,
                    "value": 10004
                },
                {
                    "id": 16,
                    "value": 10005
                },
                {
                    "id": 8,
                    "value": 10006
                },
                {
                    "id": 17,
                    "value": 10007
                },
                {
                    "id": 18,
                    "value": 10008
                },
                {
                    "id": 19,
                    "value": 10009
                },
                {
                    "id": 20,
                    "value": 10010
                },
                {
                    "id": 9,
                    "value": 10011
                },
                {
                    "id": 21,
                    "value": 10012
                },
                {
                    "id": 22,
                    "value": 10013
                },
                {
                    "id": 23,
                    "value": 10014
                },
                {
                    "id": 24,
                    "value": 10015
                },
                {
                    "id": 10,
                    "value": 10016
                },
                {
                    "id": 25,
                    "value": 10017
                },
                {
                    "id": 26,
                    "value": 10018
                },
                {
                    "id": 27,
                    "value": 10019
                },
                {
                    "id": 11,
                    "value": 20001
                },
                {
                    "id": 28,
                    "value": 20002
                },
                {
                    "id": 29,
                    "value": 20003
                },
                {
                    "id": 30,
                    "value": 20004
                },
                {
                    "id": 31,
                    "value": 20005
                },
                {
                    "id": 32,
                    "value": 20006
                },
                {
                    "id": 33,
                    "value": 20007
                },
                {
                    "id": 34,
                    "value": 20008
                },
                {
                    "id": 35,
                    "value": 20009
                },
                {
                    "id": 36,
                    "value": 20010
                },
                {
                    "id": 12,
                    "value": 30001
                }
            ]
        },
        "battle/MsgSOutput/MsgSOutput": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "data",
                    "type": {
                        "type": "Any"
                    }
                }
            ]
        },
        "MsgStr/MsgStr": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "str",
                    "type": {
                        "type": "String"
                    }
                }
            ]
        },
        "PtlAddData/ReqAddData": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "content",
                    "type": {
                        "type": "String"
                    }
                }
            ]
        },
        "PtlAddData/ResAddData": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "time",
                    "type": {
                        "type": "Date"
                    }
                }
            ]
        },
        "PtlGetData/ReqGetData": {
            "type": "Interface"
        },
        "PtlGetData/ResGetData": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "data",
                    "type": {
                        "type": "Array",
                        "elementType": {
                            "type": "Interface",
                            "properties": [
                                {
                                    "id": 0,
                                    "name": "content",
                                    "type": {
                                        "type": "String"
                                    }
                                },
                                {
                                    "id": 1,
                                    "name": "time",
                                    "type": {
                                        "type": "Date"
                                    }
                                }
                            ]
                        }
                    }
                }
            ]
        }
    }
};