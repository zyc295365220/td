export interface MsgCSellTower {
    id: number,
    /** 售出的塔id */
    towerId: number,
}
