import { EArea, EEntity } from "../../common/Enum";
/** 抽取卡片结果 */
export interface MsgSDrawCardResoult {
    area: EArea,
    /** 抽卡是否成功 */
    isSuccess: 0 | 1,
    /** 抽取到的品质 */
    quality: number,
    /** 抽取到的塔类型 */
    towerEntity: EEntity,
}