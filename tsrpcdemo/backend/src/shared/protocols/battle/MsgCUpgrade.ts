import { EUpgradeType } from "../../common/Enum";

export interface MsgCUpgrade {
    id: number,
    type: EUpgradeType,
}
