export interface MsgCDrawCard {
    /** 用户id */
    id: number,
    /** 抽取的品质 */
    quality: number,
}
