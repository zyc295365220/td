export interface MsgCExchangePos {
    id: number,
    /** 塔的位置 */
    index1: number,
    /** 移动目标位置 */
    index2: number,
}

