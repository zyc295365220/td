export interface MsgCMergeTower {
    id: number,
    /** 合成位置x坐标 */
    indexX: number,
    /** 合成位置y坐标 */
    indexY: number,
}
