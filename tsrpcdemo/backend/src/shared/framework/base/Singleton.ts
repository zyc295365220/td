export class Singleton {
    private static _ins: any = null
    protected static GetInstance<T>(): T {
        if (!this._ins) {
            this._ins = new this()
        }
        return this._ins
    }
}